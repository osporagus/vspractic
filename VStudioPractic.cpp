﻿
#include <iostream>
#include <string>

using namespace std;

int main()
{
    string s1 = "unreal_";
    string s2 = s1;
    string s3(s1 + "engine");

    string s4(s2 + s3);
        cout << s4 << endl;

    cout << s4.size() << endl;
    cout << s4.front() << endl;
    cout << s4.back() << endl;

    return 0;
}